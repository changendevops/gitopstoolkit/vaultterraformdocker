# VaultTerraformDocker

## What is it ?

This repo si composed with a minimum git to be able to use or discover Hashicorp Vault.
You can found :
- Docker recipe
- Terraform plan with modules to create approle and kv (version2)
- Python sample to fetch or push secret.

## docker-compose

Basic and simple docker-compose to start a Vault with RAFT storage backend

## Terraform plan

### module approle
this module allow you to create an approle in Vault. it need somes variables :
- mount_point : approle mount point
- path : allowed path for the approle
- access_type : can be "read", "write" et "all"
- config : if set to true, the approle will be allowed to manage the full kv
- cidr_list : list of ip/block allowed to use this approle

### module kv2
this module allow you to create a new kv. It need 2 variables :
- path : Name of the kv
- description : KV Description 

## Python sample

Using the cnd_vault lib, you can found 3 files :
- read.py
- write.py
- list.py
