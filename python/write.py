import cndprint
import vault

level = "Info"
silent_mode = True
_print = cndprint.CndPrint(level=level, silent_mode=silent_mode)
host_url = "http://localhost:8200"
role_id = "23ee8f98-0a1e-c7a5-b965-192d65604d31"
secret_id = "2222355c-52ec-de06-d6cd-6a4c68c03ad5"

_vault = vault.Vault(host_url, role_id, secret_id, _print)
secret = _vault.v2.write('denis', 'tot/abc', {'aa': 'bb'})