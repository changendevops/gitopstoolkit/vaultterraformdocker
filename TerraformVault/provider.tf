terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.14.0"
    }
  }
}

provider "vault" {
  address = "http://localhost:8200"
  token = "hvs.CXljcPgykcRwe7iII92jPqKS"
}

resource "vault_auth_backend" "approle" {
  type = "approle"
}
# vault list auth/approle/role
# vault read auth/approle/role/mysuperapp/role-id
#     role_id 609985ff-ce8f-17bd-6275-116b43b4befb
# vault write -force auth/approle/role/mysuperapp/secret-id
#     secret_id          2860a3c9-5dbe-049f-ce05-40046c2c6d26