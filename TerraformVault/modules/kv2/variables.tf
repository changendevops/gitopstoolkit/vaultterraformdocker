variable "path" {
  type        = string
  description = "Name of the KV"
}

variable "description" {
  type        = string
  description = "Desription of the KV"
}