resource "vault_mount" "this" {
  path        = var.path
  type        = "kv"
  options     = { version = "2" }
  description = var.description
}

resource "vault_kv_secret_v2" "this" {
  mount                      = vault_mount.this.path
  name                       = "tot/default secret"
  cas                        = 1
  delete_all_versions        = true
  data_json                  = jsonencode(
  {
    zip       = "zap",
    foo       = "bar"
  }
  )
  custom_metadata {
    max_versions = 5
    data = {
      foo = "vault@example.com",
      bar = "12345"
    }
  }
}