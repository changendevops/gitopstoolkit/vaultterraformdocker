locals {
  access_mode = {
    all = ["create", "read", "update", "delete", "list"]
    read = ["read", "list"]
    write = ["create", "update", "delete"]
  }
}

data "vault_auth_backend" "this" {
  path = "approle"
}

data "vault_policy_document" "config" {
  rule {
    path         = "${var.mount_point}/data/*"
    capabilities = lookup(local.access_mode, var.access_type)
    description  = "allow on kv/data/path"
  }
  rule {
    path         = "${var.mount_point}/metadata/*"
    capabilities = lookup(local.access_mode, var.access_type)
    description  = "allow on kv/metadata/path"
  }
  rule {
    path         = "${var.mount_point}/*"
    capabilities = lookup(local.access_mode, var.access_type)
    description  = "allow on kv/path"
  }
}

data "vault_policy_document" "this" {
  rule {
    path         = "${var.mount_point}/data/${var.path}/*"
    capabilities = lookup(local.access_mode, var.access_type)
    description  = "allow on kv/data/path"
  }
  rule {
    path         = "${var.mount_point}/metadata/${var.path}/*"
    capabilities = lookup(local.access_mode, var.access_type)
    description  = "allow on kv/metadata/path"
  }
}

resource "vault_policy" "this" {
  name   = replace("${var.mount_point}-${var.path}", "/", "-")
  policy = var.config ? data.vault_policy_document.config.hcl : data.vault_policy_document.this.hcl
}

resource "vault_approle_auth_backend_role" "this" {
  backend        = data.vault_auth_backend.this.path
  role_name      = replace("${var.mount_point}-${var.path}", "/", "-")
  secret_id_bound_cidrs = var.cidr_list
  token_policies = [vault_policy.this.id]
}

resource "vault_approle_auth_backend_role_secret_id" "this" {
  count = 1
  backend   = data.vault_auth_backend.this.path
  role_name = vault_approle_auth_backend_role.this.role_name
}

resource "vault_approle_auth_backend_login" "this" {
  count = 1
  backend   = data.vault_auth_backend.this.path
  role_id   = vault_approle_auth_backend_role.this.role_id
  secret_id = vault_approle_auth_backend_role_secret_id.this.*.secret_id[count.index]
}