variable "mount_point" {
  type        = string
  description = "KV mount point"
}

variable "path" {
  type        = string
  description = "Path"
}

variable "access_type" {
  type        = string
  description = "Define acces type for the current approle (read-only, write-only, all)"
  validation {
    condition     = contains(["read", "write", "all"], var.access_type)
    error_message = "Valid values for access_type (read, write, all)."
  }   
}

variable "config" {
  type        = bool
  description = "Allow config mode"
  default     = false
}

variable "cidr_list" {
  type        = list
  description = "List of allowed ip block"
}