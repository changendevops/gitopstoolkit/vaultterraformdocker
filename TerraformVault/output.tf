output "cnd_role_id" {
  description = "The role ID of created approle"
  value       = module.approle_cnd.role_id
}
output "cnd_secret_id" {
  description = "The role ID of created approle"
  value       = module.approle_cnd.secret_id
  sensitive   = true
}
output "test_role_id" {
  description = "The role ID of created approle"
  value       = module.approle_test.role_id
}
output "test_secret_id" {
  description = "The role ID of created approle"
  value       = module.approle_test.secret_id
  sensitive   = true
}