module "approle_cnd" {
  depends_on = [
    vault_auth_backend.approle
  ]
  source = "./modules/approle"
  mount_point = "denis"
  path        = "tot"
  access_type = "all"
  config      = true
  cidr_list   = ["172.20.0.1/32"]
}

module "approle_test" {
  depends_on = [
    vault_auth_backend.approle
  ]
  source = "./modules/approle"
  mount_point = "denis"
  path        = "tat/tot/tut"
  access_type = "write"
  cidr_list   = ["172.20.0.1/32"]
}